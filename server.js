var express = require('express');
var axios = require ('axios');

const naturalDBClient = axios.create({
  baseURL: 'https://api.natural-db.com',
  timeout: 1000,
  headers: {'Authentication': process.env.NATURAL_DB_API_TOKEN}
});
    
var app = express();
app.use(express.static('public'));


var bodyParser = require('body-parser');
app.use(bodyParser.json());
// in latest body-parser use like below.
app.use(bodyParser.urlencoded({ extended: true }));

var sassMiddleware = require("node-sass-middleware");

app.use(sassMiddleware({
  src: __dirname + '/public',
  dest: '/tmp',
  force: true
}));


app.get("/", function (request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

app.post('/add-question', function(request, response) {
  console.log(request.body.question);
    var url = "/databases/4/queries";
    var query = {
      sql: `INSERT INTO "Questions" ("Question", "Answer")
            VALUES ('${request.body.question}', '${request.body.answer}';`
    };
    var naturalRequest = naturalDBClient.post(url, query);
    naturalRequest.then((naturalResponse) => {
      response.json({status: 'ok'});
    });

    naturalRequest.catch((error) => {
      response.status(500);
    })
});


app.get('/random-question', function(request, response) {
  
  var responseData = {};
  
  var url = "/databases/4/queries";
  var query = {sql: "SELECT * FROM \"Questions\" LEFT JOIN \"Topics\" ON \"Questions\".\"Topic\" = \"Topics\".id ORDER BY RANDOM() LIMIT 1;"};
  var naturalRequest = naturalDBClient.post(url, query)
  naturalRequest.then((naturalResponse) => {
    console.log(naturalResponse.data);
    responseData.id = naturalResponse.data.result[0][0];
    responseData.question = naturalResponse.data.result[0][1];
    responseData.answer = naturalResponse.data.result[0][2];
    responseData.topic = naturalResponse.data.result[0][5];
    responseData.explanation = naturalResponse.data.result[0][6];

    response.json(responseData);
  });
  
  naturalRequest.catch((error) => {
    console.log(error);
  })
});

var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
