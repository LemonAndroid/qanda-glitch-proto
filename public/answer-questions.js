var locale = {
  "no-last-question": {
    en: "Answer a question, you can obtain the correct answer by clicking the - Last answer - Button." 
  }
}

$(function() {
  transitionToNextQuestion();
});

var currentQuestion = null;
var points = 0;

function checkAnswer() {
  var $answerInput = $('.question-card .answer input');
  
  var outcome = currentQuestion.answer === $answerInput.val();
  
  if (outcome) {
    points += 1;
  } else {
    points = 0;
  }
  
  $('.score-bar .points').text(points);

  
  $answerInput.val('');
  transitionToNextQuestion(outcome ? 'win' : 'loss');
}

function checkAnswerOnEnter(e) {
  if (e.keyCode == 13) {
    checkAnswer();
  }
}

function skipQuestion() {
  transitionToNextQuestion();
}


function transitionToNextQuestion(curtainDownFlavor = 'neutral') {
  var $questionCard = $('.question-card');
  var $questionCardContent = $questionCard.find('.question-card-inner-wrapper').detach();
  
  $questionCard.addClass('curtain-down');
  $questionCard.addClass(curtainDownFlavor);
  
  loadQuestion().then(function(nextQuestion) {
    var $question = $questionCardContent.find('.question');
    $question.empty();
    
    if(currentQuestion.topic) {
      $question.append(`<h4>${currentQuestion.topic}</h4>`);
    }
    
    if(currentQuestion.explanation) {
      $question.append(`<p>${currentQuestion.explanation}</p>`);
      $question.append('<h4>Question</h4>');
    }
    
    $question.append('<p>' + currentQuestion.question + '</p>');
    $questionCard.removeClass('curtain-down');
    $questionCard.removeClass(curtainDownFlavor)

    $questionCardContent.appendTo($questionCard);
    
    $questionCard.find('.answer input').focus();
  });
}

var lastQuestion = null;

function loadQuestion() {
  var promise = new Promise(function(resolve, reject) {
    var serverRequest = axios.get('random-question');

    serverRequest.then(function (response) {
      lastQuestion = currentQuestion;
      currentQuestion = response.data;

      resolve(currentQuestion);
    });
  });

  return promise;
}

$(window).on('hashchange', function(event) {
  switch (window.location.hash) {
      case '#last-answer':
        if (lastQuestion) {
          alert(lastQuestion.answer);
        } else {
          alert(locale['no-last-question']['en']);
        }
        window.location.hash = '';
        break;
  }
});