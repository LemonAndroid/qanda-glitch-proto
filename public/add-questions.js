class AddQuestionForm {
  constructor(targetId) {
    this.target = document.getElementById(targetId);
  }
  
  attachEventHandlers() {
    this.target.addEventListener('submit', this.onSubmit.bind(this));
  }
  
  onSubmit(e) {
    e.preventDefault();
    var formData = this.collectFormData();
    this.pushToServer(formData);
  }
  
  pushToServer(data) {
    var url = "/add-question";
    var query = {
      question: data.question,
      answer: data.answer,
      topic: data.topic,
      explanation: data.explanation
    };
    var naturalRequest = axios.post(url, query);
    naturalRequest.then((naturalResponse) => {
      this.clearInputs();
      alert('Question added');
    });

    naturalRequest.catch((error) => {
      console.log(error);
    })
  }
  
  clearInputs() {
    $(this.target).find('input').each(function(i, input) {
      // console.log(input);
      $(input).val('');
    });
  }
  
  collectFormData() {
    var fdata = {};
    fdata.question =  this.target.elements['question'].value;
    fdata.answer = this.target.elements['answer'].value;
    fdata.topic = this.target.elements['topic'].value;
    fdata.explanation = this.target.element['explanation'].value
    return fdata;
  }
}

$(function() {
  var addQuestionForm = new AddQuestionForm('add-question-form');
  addQuestionForm.attachEventHandlers();
});
